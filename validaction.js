//walidacja dla rejestracji klienta
jQueryValidate(document).ready(function(){
    jQueryValidate.validator.addMethod('lowercase', function(value) {
        return value.match(/^[^A-Z]+$/);
    },"Required a small letters");
    jQueryValidate.validator.addMethod("maxDate", function(value) {
        var now = new Date();
        var start = new Date();
        start = start.setFullYear(now.getFullYear() - 8);
        var inputDate = new Date(value);
        if (inputDate < start){
            return true;
        }
        return false;
    }, "You put invalid data of birth or You are too young.");
    //-----------------------------------------------------
    //walidacja klienta
    //----------------------------------------------------
    jQueryValidate('#register').validate({
        submitHandler:function(form) {
                    SubmittingForm();
        },
        rules: {
            name: {
                required: true,
            },
            surname: {
                required: true,
            },
            username: {
                required: true,
                lowercase: true,
            },      
            email: {                
                required: true,
                email: true
            },
            password1: {
                required: true,
                minlength: 6,
            },
            password2: {
                required: true,
                equalTo: "#password1",
                minlength: 6,
            },
            date: {
                required: true,
                date: true,
                maxDate: true
            }
            
        },
        messages: {

            name: "Required Name",
            surname: "Required Surame",
            username: { 
                required:"Required Login",

            },
            date: {
                required: "Required date of birth",
                date: "Required correct date"

            },
            email: {
                required: "Required e-mail",
                email: "Invalid e-mail format"

            },
            password1: {
                required: "Required Password",
                minlength: "The length of the password should be longer than 6 characters"

            },
            password2: {
                required: "Repeat password, please!",
                minlength: "The length of the password should be longer than 6 characters",
                equalTo:  "Invalid password"
            }

        }
    });
    //-----------------------------------
    //walidacja intytucji w rejestracji
    //-----------------------------------
    jQueryValidate('#institution').validate({
        submitHandler:function(form) {
                    SubmittingForm();
        },
        rules: {
            name_company: {
                required: true,
            },
            name: {
                required: true,
            },
            surname: {
                required: true,
            },
            city: {
                required: true,
            },
            
            email: {                
                required: true,
                email: true
            },
            date: {
                required: true,
                date: true,
                maxDate: true
            },
            login: {
                required: true,
                lowercase: true,
            },      
            password1: {
                required: true,
                minlength: 6,
            },
            password2: {
                required: true,
                equalTo: "#password1",
                minlength: 6,
            }
            
        },
        messages: {

            name_company: "Required Name Company",
            name: "Required Name",
            surname: "Required Surname",
            city: "Required City",
            email: {
                required: "Required e-mail",
                email: "Invalid e-mail format"

            },
            date: {
                required: "Required date of birth",
                date: "Required correct date"

            },
           
            login: { 
                required:"Required Login",

            },
            
            password1: {
                required: "Required Password",
                minlength: "The length of the password should be longer than 6 characters"

            },
            password2: {
                required: "Repeat password, please!",
                minlength: "The length of the password should be longer than 6 characters",
                equalTo:  "Invalid password"
            }

        }
    });
    //-------------------------------
    //walidacja zmiana hasła
    //-------------------------------
    jQueryValidate('#changePassword').validate({
        submitHandler:function(form) {
                    SubmittingForm();
        },
        rules: {
            oldpassword: {
                required: true,
                minlength: 6
            },
            
            newpassword: {
                required: true,
                minlength: 6
            },
            repeatpassword: {
                required: true,
                equalTo: "#password1",
                minlength: 6
            }
            
            
        },
        messages: {
            oldpassword: {
                required: "Required Password",
                minlength: "The length of the password should be longer than 6 characters"
            },   
            newpassword: {
                required: "Required Password",
                minlength: "The length of the password should be longer than 6 characters"

            },
            repeatpassword: {
                required: "Repeat password, please!",
                minlength: "The length of the password should be longer than 6 characters",
                equalTo:  "Invalid password"
            }

        }
    });

    //--------------------------------------------------
    //newsletter
    //-------------------------------------------------
    jQueryValidate('#form').validate({
            submitHandler:function(form) {
                        SubmittingForm();
            },
            rules: {     
                email: {                
                    required: true,
                    email: true
                }          
            },
            messages: {
                email: {
                    required: "Required e-mail",
                    email: "Invalid e-mail format"

                }
            }
    });
    //--------------------------------------------------
    //contact
    //-------------------------------------------------
    jQueryValidate('#contact').validate({
        submitHandler:function(form) {
                    SubmittingForm();
        },
        rules: {
            title: {
                required: true,
            },
            message: {
                required: true,
            },
            email: {                
                required: true,
                email: true
            }
            
            
        },
        messages: {

            title: "Required Title",
            message: "Required Message",
            email: {
                required: "Required e-mail",
                email: "Invalid e-mail format"

            }
        }
    });
    jQueryValidate('#validateFormBuyTicket').validate({
        submitHandler:function(form) {
            SubmittingForm();
        },
        rules: {
            zip: {
                minlength: 5
            },
            email: {
                email: true
            },   
            card_code: {                
                 minlength: 3
            }
        },
        messages: {
            zip: {
                minlength: "The length of the zipcode should be longer than 5 characters"

            },
            mail: {
                email: "Invalid e-mail format"

            },
            card_code: {
                minlength: "The length of the card_code should be longer than 3 characters"
            }

        }
    });
    

});