<?php
include_once('model/model.php');


class AccountModel extends Model{
	#Rejestracja klienta Zapisanie danych do SQL
	public function insertRegisterClient($data){
		$initial = $this->pdo->prepare('INSERT INTO account(login,first_name,last_name,password,date_age,city,street,mail,choose_person) VALUES (:login,
			:first_name,
			:last_name,
			sha1(:password),
			:date_age,
			:city,
			:street,
			:mail,
			:choose_person)');
        $initial->bindValue(':login', $data['login'], PDO::PARAM_STR);
        $initial->bindValue(':first_name', $data['first_name'], PDO::PARAM_STR);
        $initial->bindValue(':last_name', $data['last_name'], PDO::PARAM_STR);
        $initial->bindValue(':password', $data['password'], PDO::PARAM_STR);
        $initial->bindValue(':date_age', $data['age'], PDO::PARAM_STR);
        $initial->bindValue(':city', $data['city'], PDO::PARAM_STR);
        $initial->bindValue(':street', $data['street'], PDO::PARAM_STR);
        $initial->bindValue(':mail', $data['mail'], PDO::PARAM_STR);
        $initial->bindValue(':choose_person', false, PDO::PARAM_BOOL);
        $initial->execute();

	}
	#Sprawdzenie czy użytkownik istnieje
	public function checkLogin($login){
		$initial= $this->pdo->prepare('SELECT count(login) as size FROM account WHERE login=:login_check');
		$initial->bindValue(':login_check',$login,PDO::PARAM_STR);
		$initial->execute();
		$result = $initial->fetch();
		return $result;
	}
	#Rejestracja osoby tworząca jakieś wydarzenie
	public function insertRegisterPersonCreateEvent($data){
		$initial= $this->pdo->prepare('INSERT INTO account(login,first_name,last_name,password,date_age,city,street,mail,zipcode,company_name,choose_person) VALUES(:login,
			:first_name,
			:last_name,
			sha1(:password),
			:date_age,
			:city,
			:street,
			:mail,
			:zipcode,
			:company_name,
			:choose_person)');
		$initial->bindValue(':login', $data['login'], PDO::PARAM_STR);
        $initial->bindValue(':first_name', $data['first_name'], PDO::PARAM_STR);
        $initial->bindValue(':last_name', $data['last_name'], PDO::PARAM_STR);
        $initial->bindValue(':password', $data['password'], PDO::PARAM_STR);
        $initial->bindValue(':date_age', $data['age'], PDO::PARAM_INT);
        $initial->bindValue(':city', $data['city'], PDO::PARAM_STR);
        $initial->bindValue(':street', $data['street'], PDO::PARAM_STR);
        $initial->bindValue(':mail', $data['mail'], PDO::PARAM_STR);
        $initial->bindValue(':zipcode', $data['zipcode'], PDO::PARAM_INT);
        $initial->bindValue(':company_name', $data['name_company'], PDO::PARAM_STR);
        $initial->bindValue(':choose_person', true, PDO::PARAM_BOOL);
        $initial->execute();

	}
	#Dodawanie stron internetowych dla przedsiębiorcy
	public function insertAddSocialWeb($web){
		$initial= $this->pdo->prepare('INSERT INTO account(page_WWW,fabpage_facebook) VALUES(:page_WWW,:fanpage_facebook)');
		$initial->bindValue(':page_WWW',$web['web'],PDO::PARAM_STR);
		$initial->bindValue(':fanpage_facebook',$web['facebook'],PDO::PARAM_STR);
		$initial->execute();
	}
	#Logowanie
	public function SignInModel($login,$password){
		$initial= $this->pdo->prepare("SELECT account.id, account.choose_person FROM account WHERE login=:login and password=sha1(:password) ");
		$initial->bindValue(':login',$login,PDO::PARAM_STR);
		$initial->bindValue(':password',$password,PDO::PARAM_STR);
		$initial->execute();
		$result = $initial->fetchAll();
		return $result;

	}

	public function SelectPassword($account_id,$password){
		$initial= $this->pdo->prepare("SELECT id,login,password FROM account WHERE password=sha1(:password) AND id=:account_id ");
		$initial->bindValue(':password',$password,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);

		$initial->execute();
		$result['old_password'] = $initial->fetch(PDO::FETCH_ASSOC);
		$initial= $this->pdo->prepare("SELECT count(*) as size FROM account WHERE password=sha1(:password) AND id=:account_id ");
		$initial->bindValue(':password',$password,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
		$result['size'] = $initial->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	public function updateAccountPassword($account_id,$password){
		$initial = $this->pdo->prepare("UPDATE account SET password = sha1(:password) WHERE id = :account_id");
		$initial->bindValue(':password',$password,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}

	#aktualizacja danych osobowych imię
	public function updateAccountFirstName($first_name,$account_id){
		$initial = $this->pdo->prepare("UPDATE account SET first_name = :first_name WHERE id = :account_id");
		$initial->bindValue(':first_name',$first_name,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}
	#aktualizacje danych osobowych nazwisko
	public function updateAccountLastName($last_name,$account_id){
		$initial = $this->pdo->prepare("UPDATE account SET last_name = :last_name WHERE id = :account_id");
		$initial->bindValue(':last_name',$last_name,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}
	#aktualizacje danych osobowych login
	public function updateAccountLogin($login,$account_id){
		$initial = $this->pdo->prepare("UPDATE account SET login = :login WHERE id = :account_id");
		$initial->bindValue(':login',$login,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}
	#aktualizacje danych osobowych miasto
	public function updateAccountCity($city,$account_id){
		$initial = $this->pdo->prepare("UPDATE account SET city = :city WHERE id = :account_id");
		$initial->bindValue(':city',$city,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}
	#aktualizacje danych osobowych ulica
	public function updateAccountStreet($street,$account_id){
		$initial = $this->pdo->prepare("UPDATE account SET street = :street WHERE id = :account_id");
		$initial->bindValue(':street',$street,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}
	#aktualizacja danych osbowych e-mail
	public function updateAccountMail($mail,$account_id){
		$initial = $this->pdo->prepare("UPDATE account SET mail = :mail WHERE id = :account_id");
		$initial->bindValue(':mail',$mail,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}
	#aktualizacja danych osbowych date
	public function updateAccountDateAge($date,$account_id){
		$initial = $this->pdo->prepare("UPDATE account SET date_age = :date_age WHERE id = :account_id");
		$initial->bindValue(':date_age',$date,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}
	#aktualizacja danych osbowych phone
	public function updateAccountPhone($phone,$account_id){
		$initial = $this->pdo->prepare("UPDATE account SET phone = :phone WHERE id = :account_id");
		$initial->bindValue(':phone',$phone,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}
	#aktualizacja danych osbowych zipcode
	public function updateAccountZipCode($zipcode,$account_id){
		$initial = $this->pdo->prepare("UPDATE account SET zipcode = :zipcode WHERE id = :account_id");
		$initial->bindValue(':zipcode',$zipcode,PDO::PARAM_STR);
		$initial->bindValue(':account_id',$account_id,PDO::PARAM_INT);
		$initial->execute();
	}
	#Wyświetlenie danych ososbowych klienta
	public function selectAccountClient($id,$choose_person){
		$initial= $this->pdo->prepare("SELECT login,first_name,last_name,date_age,city,street,mail,zipcode FROM account WHERE id=:id and choose_person=:choose_person");
		$initial->bindValue(":id",$id,PDO::PARAM_INT);
		$initial->bindValue(":choose_person",$choose_person,PDO::PARAM_BOOL);
		$initial->execute();
		$result = $initial->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	#Wyświetlenie danych ososbowych instytucji
	public function selectAccountInstitution($id,$choose_person){
		$initial= $this->pdo->prepare("SELECT company_name,login,first_name,last_name,date_age,city,street,zipcode,mail,phone,page_WWW,fanpage_facebook FROM account WHERE id=:id and choose_person=:choose_person");
		$initial->bindValue(":id",$id,PDO::PARAM_INT);
		$initial->bindValue(":choose_person",$choose_person,PDO::PARAM_BOOL);
		$initial->execute();
		$result = $initial->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
}

?>