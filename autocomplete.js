$(document).ready(function(){
	//Autocomplete search
  $( "#title_search" ).autocomplete({
    source: function( request, response ) {
		    $.ajax({
		      type: "GET",
		      url: '/calendar/api/',
		      dataType: "json",
				  data: {
					 title_search: request.term,
				  },
				  complete: function(data){
					 if(data.status = '200'){
              			var data =  JSON.parse(JSON.stringify(eval('(' + data.responseText+ ')')));
              			response($.map(data, function (item) {
                			return {
                  				label: item.title, 
                  				value: item.title
                			};
              			}));
					  }  
    		  }
		    });
		},
    minLength: 2
  });
  $( "#title_search_category" ).autocomplete({
    source: function( request, response ) {
		    $.ajax({
		      type: "GET",
		      url: '/calendar/api/',
		      dataType: "json",
				  data: {
					 title_search: request.term,
				  },
				  complete: function(data){
					if(data.status = '200'){
              			var data =  JSON.parse(JSON.stringify(eval('(' + data.responseText+ ')')));
              			response($.map(data, function (item) {
                			return {
                  				label: item.title, 
                  				value: item.title
                			};
              			}));
					  }  
    		  		}
		    	});
		},
    minLength: 2
  });


});
